<?php
namespace Mageplaza\HelloWorld\Block;
class Display extends \Magento\Framework\View\Element\Template
{

    public $title;
    public $description;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context)
    {
        parent::__construct($context);

        $this->title = 'Hello World';
        $this->description = 'this is one description';
    }

    public function sayHello()
    {
//        return __('Hello World');
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }
}