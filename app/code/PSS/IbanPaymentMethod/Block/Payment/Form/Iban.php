<?php
/**
 * @author Israel Yasis
 */
namespace PSS\IbanPaymentMethod\Block\Payment\Form;

/**
 * Block for Iban payment method form
 */
class Iban extends \Magento\OfflinePayments\Block\Form\AbstractInstruction
{
    /**
     * Iban template
     *
     * @var string
     */
    protected $_template = 'payment/form/iban.phtml';
}