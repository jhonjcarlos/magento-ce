<?php
/**
 * @author Israel Yasis
 */
namespace PSS\IbanPaymentMethod\Block\Payment\Info;

class Iban extends \Magento\Payment\Block\Info
{
    /**
     * @var string
     */
    protected $_template = 'PSS_IbanPaymentMethod::payment/info/iban.phtml';

    /**
     * @return string
     */
    public function toPdf()
    {
        $this->setTemplate('PSS_IbanPaymentMethod::payment/info/pdf/iban.phtml');
        return $this->toHtml();
    }
}