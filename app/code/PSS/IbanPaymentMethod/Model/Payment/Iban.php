<?php
/**
 * @author Israel Yasis
 */
namespace PSS\IbanPaymentMethod\Model\Payment;

class Iban extends \Magento\Payment\Model\Method\AbstractMethod {

    const PAYMENT_METHOD_IBAN_CODE = 'iban_payment';
    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_METHOD_IBAN_CODE;

    /**
     * @var string
     */
    protected $_formBlockType = \PSS\IbanPaymentMethod\Block\Payment\Form\Iban::class;

    /**
     * @var string
     */
    protected $_infoBlockType = \PSS\IbanPaymentMethod\Block\Payment\Info\Iban::class;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * Assign data to info model instance
     *
     * @param \Magento\Framework\DataObject|mixed $data
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        $this->getInfoInstance()->setPoNumber($data->getPoNumber());
        return $this;
    }
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null) {
        $isAvailable = parent::isAvailable($quote);
        return $isAvailable;
    }
}
