/**
 * @author Israel Yasis
 */
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'iban_payment',
                component: 'PSS_IbanPaymentMethod/js/view/payment/method-renderer/iban-method'
            }
        );
        return Component.extend({});
    }
);
